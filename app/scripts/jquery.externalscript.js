jQuery.externalScript = function(url, options) {
  'use strict';	
  options = $.extend(options || {}, {
    dataType: "script",
    cache: true,
    url: url
  });
  return jQuery.ajax(options);
};
