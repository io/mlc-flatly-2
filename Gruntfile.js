// Generated on 2014-08-28 using generator-webapp 0.4.9
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);  
    //grunt.loadNpmTasks('grunt-contrib-haml');

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    // Configurable paths
    var config = {
        app: 'app',
        dist: 'dist',
        src: 'src'
    };

    // Define the configuration for all the tasks
    grunt.initConfig({

        // Project settings
        config: config,

        // Watches files for changes and runs tasks based on the changed files
        watch: {
            bower: {
                files: ['bower.json'],
                tasks: ['bowerInstall']
            },
            js: {
                files: ['<%= config.app %>/scripts/{,*/}*.js'],
                tasks: ['jshint'],
                options: {
                    livereload: true
                }
            },
            jstest: {
                files: ['test/spec/{,*/}*.js'],
                tasks: ['test:watch']
            },
            gruntfile: {
                files: ['Gruntfile.js']
            },
            sass: {
                files: ['<%= config.src %>/sass/{,*/}*.{scss,sass}'],
                tasks: ['sass','sass:server', 'autoprefixer'],
                options: {
                    livereload: true
                }                
            },
            styles: {
                files: ['<%= config.app %>/styles/{,*/}*.css'],
                tasks: ['newer:copy:styles', 'autoprefixer'],
                options: {
                    livereload: true
                }                
            },            
            coffee: {
                files: ['<%= config.src %>/coffeescript/{,*/}*.coffee'],
                tasks: ['coffee'],
                options: {
                    livereload: true
                }                
            }, 
            haml: {
               files: ['<%= config.src %>/haml/*.haml'],
               tasks: ['haml'],
                options: {
                    livereload: true
                }                 
            },            
            livereload: {
                options: {
                    livereload: '<%= connect.options.livereload %>'
                },
                files: [
                    '<%= config.app %>/{,*/}*.{html, haml}',
                    '.tmp/styles/{,*/}*.css',
                    '<%= config.app %>/images/{,*/}*'
                ]
            },           
        },
        haml: {
          dist: {
            files: {          	  
              '<%= config.app %>/index.html': '<%= config.src %>/haml/index.haml',
              '<%= config.app %>/docs.html': '<%= config.src %>/haml/docs.haml',
              '<%= config.app %>/demo.html': '<%= config.src %>/haml/demo.haml'              
            },
          },
        },
        // The actual grunt server settings
        connect: {
            options: {
                port: 9000,
                open: true,
                livereload: 35729,
                // Change this to '0.0.0.0' to access the server from outside
                hostname: '0.0.0.0'
            },
            livereload: {
                options: {
                    middleware: function(connect) {
                        return [
                            connect.static('.tmp'),
                            connect().use('/bower_components', connect.static('./bower_components')),
                            connect.static(config.app)
                        ];
                    }
                }
            },
            test: {
                options: {
                    open: false,
                    port: 9001,
                    middleware: function(connect) {
                        return [
                            connect.static('.tmp'),
                            connect.static('test'),
                            connect().use('/bower_components', connect.static('./bower_components')),
                            connect.static(config.app)
                        ];
                    }
                }
            },
            dist: {
                options: {
                    base: '<%= config.dist %>',
                    livereload: false
                }
            }
        },

        // Empties folders to start fresh
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '.tmp',
                        '<%= config.dist %>/*',
                        '!<%= config.dist %>/.git*'
                    ]
                }]
            },
            server: '.tmp'
        },

        // Make sure code styles are up to par and there are no obvious mistakes
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            },
            all: [
                'Gruntfile.js',
                '<%= config.app %>/scripts/{,*/}*.js',
                '!<%= config.app %>/scripts/vendor/*',
                'test/spec/{,*/}*.js'
            ]
        },

        // Mocha testing framework configuration options
        mocha: {
            all: {
                options: {
                    run: true,
                    urls: ['http://<%= connect.test.options.hostname %>:<%= connect.test.options.port %>/index.html']
                }
            }
        },

        // Compiles Sass to CSS and generates necessary files if requested
        sass: {
            options: {
                loadPath: [
                    'bower_components/bourbon/dist',
                    'bower_components/color-me-sass'
                ]
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= config.src %>/sass',
                    src: ['*.scss', '*.sass'],
                    dest: '.tmp/styles',
                    ext: '.css'
                }]
            },
            server: {
                files: [{
                    expand: true,
                    cwd: '<%= config.src %>/sass',
                    src: ['*.scss', '*.sass'],
                    dest: '<%= config.app %>/styles',
                    ext: '.css'
                }]
            }
        },
        coffee: {
           compile: {
             options: {
               bare: true
           },
           files: [{
             expand: true,
             cwd: '<%= config.src %>/coffeescript',
             src: ['**/*.coffee', '**/*.js'],
             dest: '<%= config.app %>/scripts',
             ext: '.js'
           }]
           }
        }, 
        // Add vendor prefixed styles
        autoprefixer: {
            options: {
                browsers: ['last 1 version']
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: '.tmp/styles/',
                    src: '{,*/}*.css',
                    dest: '.tmp/styles/'
                }]
            }
        },

        // Automatically inject Bower components into the HTML file
        bowerInstall: {
            app: {
                src: ['<%= config.app %>/index.html']
            },
            sass: {
                src: ['<%= config.app %>/styles/{,*/}*.{scss,sass}']
            }
        },

        // Renames files for browser caching purposes
        rev: {
            dist: {
                files: {
                    src: [
                        '<%= config.dist %>/scripts/{,*/}*.js',
                        '<%= config.dist %>/styles/{,*/}*.css',
                        '<%= config.dist %>/images/{,*/}*.*',
                        '<%= config.dist %>/styles/fonts/{,*/}*.*',
                        '<%= config.dist %>/*.{ico,png}'
                    ]
                }
            }
        },

        // Reads HTML for usemin blocks to enable smart builds that automatically
        // concat, minify and revision files. Creates configurations in memory so
        // additional tasks can operate on them
        useminPrepare: {
            options: {
                dest: '<%= config.dist %>'
            },
            html: '<%= config.app %>/index.html'
        },

        // Performs rewrites based on rev and the useminPrepare configuration
        usemin: {
            options: {
                assetsDirs: ['<%= config.dist %>', '<%= config.dist %>/images']
            },
            html: ['<%= config.dist %>/{,*/}*.html'],
            css: ['<%= config.dist %>/styles/{,*/}*.css']
        },

        // The following *-min tasks produce minified files in the dist folder
        imagemin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= config.app %>/images',
                    src: '{,*/}*.{gif,jpeg,jpg,png}',
                    dest: '<%= config.dist %>/images'
                }]
            }
        },

        svgmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= config.app %>/images',
                    src: '{,*/}*.svg',
                    dest: '<%= config.dist %>/images'
                }]
            }
        },

        htmlmin: {
            dist: {
                options: {
                    collapseBooleanAttributes: true,
                    collapseWhitespace: true,
                    removeAttributeQuotes: true,
                    removeCommentsFromCDATA: true,
                    removeEmptyAttributes: true,
                    removeOptionalTags: true,
                    removeRedundantAttributes: true,
                    useShortDoctype: true
                },
                files: [{
                    expand: true,
                    cwd: '<%= config.dist %>',
                    src: '{,*/}*.html',
                    dest: '<%= config.dist %>'
                }]
            }
        },

        // By default, your `index.html`'s <!-- Usemin block --> will take care of
        // minification. These next options are pre-configured if you do not wish
        // to use the Usemin blocks.
        cssmin: {
            dist: {
                files: {
                    '<%= config.dist %>/styles/flatly-2.min.css': [  
                    	'bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.css',
                    	'bower_components/prism/themes/prism.css',
                        'bower_components/prism/themes/prism-coy.css',
                        'bower_components/prism/plugins/line-numbers/prism-line-numbers.css',
                        '<%= config.app %>/styles/prism-haml.css',
                        'bower_components/nprogress/nprogress.css',    
                        //'bower_components/select2/select2.css',
                        'bower_components/iCheck/skins/square/blue.css',
                        'bower_components/iCheck/skins/square/green.css',
                        'bower_components/blueimp-file-upload/css/jquery.fileupload.css',
                        'bower_components/ladda/dist/ladda-themeless.min.css',
                        'bower_components/Strength.js/src/strength.css',
                        'bower_components/stacktable.js/stacktable.css',
                        'bower_components/bootstrap-daterangepicker/daterangepicker-bs3.css',
                        'bower_components/bootstrap-sortable/Contents/bootstrap-sortable.css',
                        //'bower_components/tablesorter/themes/blue/style.css',
                        '<%= config.app %>/styles/strength.css',
                        '<%= config.app %>/styles/main.css'
                    ],                   
                    '<%= config.dist %>/styles/flatly-2-theme.min.css': [
                        'bower_components/FlexSlider/flexslider.css',                        
                        'bower_components/jmpress.js/dist/basic-animations.css',
                        '<%= config.app %>/styles/public.css'
                    ]                    
                }
            }
        },
        uglify: {
            dist: {
                files: {
                    '<%= config.dist %>/scripts/flatly-2.min.js': [ 
                        '<%= config.app %>/scripts/jquery.externalscript.js',                     	    
                        'bower_components/classie/classie.js',
                        'bower_components/moment/moment.js',
                        'bower_components/jquery-metadata/jquery.metadata.js',
                        'bower_components/jquery.easing/jquery.easing.1.3.js',
                        'bower_components/jquery.transit/jquery.transit.js',
                        'bower_components/prism/prism.js',   
                        'bower_components/prism/plugins/line-numbers/prism-line-numbers.js',
                        'bower_components/prism/components/prism-ruby.js',
                        'bower_components/prism/components/prism-sql.js',
                        'bower_components/prism/components/prism-scss.js',
                        'bower_components/prism/components/prism-javascript.js',
                        'bower_components/prism/components/prism-css.js',
                        'bower_components/prism/components/prism-coffeescript.js',
                        'bower_components/prism/components/prism-bash.js',
                        'bower_components/prism/components/prism-markup.js',
                        '<%= config.app %>/scripts/prism-haml.js',                        
                        'bower_components/iCheck/icheck.js',
                        'bower_components/select2/select2.js',
                        'bower_components/nprogress/nprogress.js',
                        'bower_components/slimScroll/jquery.slimscroll.js',
                        'bower_components/blueimp-file-upload/js/vendor/jquery.ui.widget.js',
                        'bower_components/blueimp-file-upload/js/jquery.fileupload.js',
                        'bower_components/blueimp-file-upload/js/jquery.iframe-transport.js',
                        'bower_components/ladda/js/spin.js',
                        'bower_components/ladda/js/ladda.js',
                        'bower_components/Strength.js/src/strength.js',
                        'bower_components/stacktable.js/stacktable.js',
                        'bower_components/bootstrap-daterangepicker/daterangepicker.js',
                        'bower_components/jquery.payment/lib/jquery.payment.js',  
                        'bower_components/bootstrap-sortable/Scripts/bootstrap-sortable.js',
                        //'bower_components/tablesorter/jquery.tablesorter.js',
                        'bower_components/typeahead.js/dist/typeahead.bundle.js',                        
                        'bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.js'
                    ],
                    '<%= config.dist %>/scripts/flatly-2-theme.min.js': [ 
                        'bower_components/FlexSlider/jquery.flexslider.js',                        
                        'bower_components/jmpress.js/dist/jmpress.js',
                        'bower_components/jquery-imageloader/jquery.imageloader.js',
                        'bower_components/Vague.js/Vague.js',
                        'bower_components/jquery.BlackAndWhite/jquery.BlackAndWhite.js',
                        '<%= config.app %>/scripts/jmslideshow.js'
                    ]                    
                }
            }
        },
        concat: {
            dist: {}
        },

        // Copies remaining files to places other tasks can use
        copy: {
            dist: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.app %>',
                    dest: '<%= config.dist %>',
                    src: [
                        '*.{ico,png,txt}',
                        '.htaccess',
                        'img/{,*/}*.*',
                        '{,*/}*.html',
                        'fonts/{,*/}*.*',
                        'favicons/*.*'
                    ]}]
            },
            styles: {
                expand: true,
                dot: true,
                cwd: '<%= config.app %>/styles',
                dest: '.tmp/styles/',
                src: '{,*/}*.css'
            }
        },

        // Run some tasks in parallel to speed up build process
        concurrent: {
            server: [
                'sass:server',
                'copy:styles'
            ],
            test: [
                'copy:styles'
            ],
            dist: [
                'sass',
                'copy:styles',
                'imagemin',
                'svgmin'
            ]
        }
    });


    grunt.registerTask('serve', function (target) {
        if (target === 'dist') {
            return grunt.task.run(['build', 'connect:dist:keepalive']);
        }

        grunt.task.run([
            'clean:server',
            'concurrent:server',
            'autoprefixer',
            'connect:livereload',
            'haml',
            'sass',
            'coffee',
            'watch'
        ]);
    });

    grunt.registerTask('server', function (target) {
        grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
        grunt.task.run([target ? ('serve:' + target) : 'serve']);
    });

    grunt.registerTask('test', function (target) {
        if (target !== 'watch') {
            grunt.task.run([
                'clean:server',
                'concurrent:test',
                'autoprefixer'
            ]);
        }

        grunt.task.run([
            'connect:test',
            'mocha'
        ]);
    });

    grunt.registerTask('build', [
        'clean:dist',
        'useminPrepare',
        'concurrent:dist',
        'autoprefixer',
        'concat',
        'cssmin',
        'uglify',
        'copy:dist',
        //'rev',
        'usemin',
        'htmlmin'
    ]);

    grunt.registerTask('default', [
        'newer:jshint',
        'test',
        'build'
    ]);
    
    grunt.registerTask('default', ['jshint', 'haml']);
};
